import React from 'react'
import { shallow } from 'enzyme'
import { expect } from 'chai'

import Main from '../app/components/Main'

const wrapper = shallow(<Main />)

describe('(Component) Main', () => {
  it('renders without exploding', () => {
    expect(wrapper).to.have.length(1);
  })
  it('renders two anchor buttons', () => {
  	expect(wrapper.find('a')).to.have.length(2)
  })
  it('renders a title', () => {
  	expect(wrapper.find('h1')).to.have.length(1)
  })
})
